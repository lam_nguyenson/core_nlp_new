import pandas as pd
import numpy as np
import re

regex_train = "train_[0-9]*[\s\S]*?\"\n[0|1]"
regex_test = "test_[0-9]*[\s\S]*?\"\n"

# Load train data
with open("./sa_demo/train.crash", "r") as f:
    lines = f.read()

# Find trainning data with regex pattern
train = re.findall(regex_train, lines)

# Split to ids, labels, comments
train_ids = [t.split("\n")[0] for t in train]
train_labels = [t.split("\n")[-1] for t in train]
train_comments = ["\n".join(t.split("\n")[1:-1]) for t in train]
train_comments = [t[1:-1] for t in train_comments]
assert len(train_ids) == len(train_labels) == len(train_comments)

# Create dataframe
train_df = pd.DataFrame(
    {
        "id": train_ids,
        "comment": train_comments,
        "label": train_labels
    }
)
# Save
train_df.to_csv("./data/train.csv", index=False)

# Load train data
with open("./sa_demo/test.crash", "r") as f:
    lines = f.read()

# Find test data with regex pattern
test = re.findall(regex_test, lines)

# Split to ids, labels, comments
test_ids = [t.split("\n")[0] for t in test]
test_comments = ["\n".join(t.split("\n")[1:]) for t in test]
test_comments = [t[1:-2] for t in test_comments]
assert len(test_ids) == len(test_comments)

# Create dataframe
test_df = pd.DataFrame(
    {
        "id": test_ids,
        "comment": test_comments
    }
)

# Save
test_df.to_csv("./data/test.csv", index=False)