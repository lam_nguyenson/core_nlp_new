from word_embedding.word2vec_gensim import train
data_path="data/word_embedding/real/training"
model_path="models/word2vec_demo.model"
train(data_path=data_path, model_path=model_path)