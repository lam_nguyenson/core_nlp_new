from tokenization.crf_tokenizer import CrfTokenizer
from word_embedding.word2vec_gensim import Word2Vec
from text_classification.short_text_classifiers import BiDirectionalLSTMClassifier, load_synonym_dict

# Please give the correct paths
# Load word2vec model from file. If you want to train your own model, please go to README or check word2vec_gensim.py
word2vec_model = Word2Vec.load('models/pretrained_word2vec.bin')

# Load tokenizer model for word segmentation. If you want to train you own model,
# please go to README or check crf_tokenizer.py
tokenizer = CrfTokenizer(config_root_path='tokenization/',
                         model_path='models/pretrained_tokenizer.crfsuite')
sym_dict = load_synonym_dict('data/sentiment/synonym.txt')
print(sym_dict)
keras_text_classifier = BiDirectionalLSTMClassifier(tokenizer=tokenizer, word2vec=word2vec_model.wv,
                                                    model_path='models/sentiment_model.h5',
                                                    max_length=100, n_epochs=10,
                                                    sym_dict=sym_dict)
# # print(keras_text_classifier)
# # Load and prepare data
X, y = keras_text_classifier.load_data(['data/sentiment/samples/positive.txt',
                                       'data/sentiment/samples/negative.txt'],
                                       load_method=keras_text_classifier.load_data_from_file)

# # Train your classifier and test the model
keras_text_classifier.train(X, y)
label_dict = {0: 'tích cực', 1: 'tiêu cực'}
testsite_array = []
import re
regexp = re.compile(r'test_0')
with open('test.crash') as my_file:
    comment = ''
    for line in my_file:
        if regexp.search(line):
            if (comment):
                # print(comment)
                testsite_array.append(comment)
            comment = ''
        elif ((line != '\n') and line):
            comment = comment + line.replace('"', '').replace('\n', '')

    if (comment):
        testsite_array.append(comment)
# print(testsite_array)
print(len(testsite_array))
# test_sentences = ['Đóng gói sản phẩm rất đẹp và chắc chắn Rất đáng tiền mỗi tội dao hàng hơi muộn', 'Hay thế', 'phim chán thật', 'nhảm quá', 'chán quá', 'dở quá', 'không hay']
labels = keras_text_classifier.classify(testsite_array, label_dict=label_dict)
# print(labels)  # Output: ['tiêu cực', 'tích cực', 'tiêu cực', 'tiêu cực']
print(str(len(labels)))

results = []
index = 0
for label in labels:
    if (label == 'tích cực'):
        if (index < 10):
            results.append('test_00000{0}, 0'.format(index))

        if ((index >= 10) and (index < 100)):
            results.append('test_0000{0}, 0'.format(index))

        if ((index >= 100) and (index < 1000)):
            results.append('test_000{0}, 0'.format(index))

        if ((index >= 1000) and (index < 10000)):
            results.append('test_00{0}, 0'.format(index))

        if ((index >= 10000) and (index < 100000)):
            results.append('test_0{0}, 0'.format(index))
    else:
        if (index < 10):
            results.append('test_00000{0}, 1'.format(index))

        if ((index >= 10) and (index < 100)):
            results.append('test_0000{0}, 1'.format(index))

        if ((index >= 100) and (index < 1000)):
            results.append('test_000{0}, 1'.format(index))

        if ((index >= 1000) and (index < 10000)):
            results.append('test_00{0}, 1'.format(index))

        if ((index >= 10000) and (index < 100000)):
            results.append('test_0{0}, 1'.format(index))
    index += 1

# print(results)
f= open("n_n_submition.csv","w+")
f.write('id,label\r\n')
for line in results:
     f.write("{0}\r\n".format(line))
f.write('test_010980, 1\r\n')


# current_syllable_id = 0
# while current_syllable_id and not done {
#     check if “word word+1 word+2” is in tri-gram-dictionary
#     => {
#         take word_word1_word2;
#         current_syllable_id += 3
#         } else if “word word+1” is in bi-gram-dictionary => {
#             take word_word1;
#             current_syllable_id += 2
#         } else => {
#             take word only;
#             current_syllable_id += 1
#         }
# }