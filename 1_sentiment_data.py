testsite_array = []
negative_data_path = 'data/sentiment/samples/negative_data.txt'
positive_data_path = 'data/sentiment/samples/positive_data.txt'
import re
negative_data_path_file= open(negative_data_path, "w+")
positive_data_path_file= open(positive_data_path, "w+")
regexp = re.compile(r'train_0')
with open('train.crash') as my_file:
    comment = ''
    for line in my_file:
        line = line.strip()
        if regexp.search(line):
            comment = ''
        elif (line == '0'):
            print(comment)
            if (comment):
                positive_data_path_file.write('{0}\r\n'.format(comment))
        elif (line == '1'):
            if (comment):
                negative_data_path_file.write('{0}\r\n'.format(comment))
        elif (line != '\n'):
            comment = comment + ' ' + line.replace('"', '').replace('\n', '')